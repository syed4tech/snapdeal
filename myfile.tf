terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
subscription_id = "716f515b-814e-467b-8915-e7759460cada"
features {}
}

# Create a resource group
resource "azurerm_resource_group" "snapdeal" {
  name     = "snapdeal"
  location = "Central India"
}
resource "azurerm_virtual_network" "snapdeal" {
  name                = "snapdeal-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.snapdeal.location
  resource_group_name = azurerm_resource_group.snapdeal.name
}

resource "azurerm_subnet" "snapdeal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.snapdeal.name
  virtual_network_name = azurerm_virtual_network.snapdeal.name
  address_prefixes     = ["10.0.2.0/24"]
}
resource "azurerm_network_interface" "snapdeal" {
  name                = "snapdeal-nic"
  location            = azurerm_resource_group.snapdeal.location
  resource_group_name = azurerm_resource_group.snapdeal.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snapdeal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "azurerm_public_ip.snapdeal.id
  }
}

resource "azurerm_linux_virtual_machine" "snapdeal" {
  name                = "snapdeal-machine"
  resource_group_name = azurerm_resource_group.snapdeal.name
  location            = azurerm_resource_group.snapdeal.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.snapdeal.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("/root/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}